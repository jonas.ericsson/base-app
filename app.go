package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", PingHandler)
	http.ListenAndServe(":5000", nil)
}

func PingHandler(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprint(writer, "Pong!")
}
